/*  engine.js
 *
 *  Description:
 *      This file contains some javascript objects and funtions tha make the
 *      game possible. Due to the grid-based nature of this engine, most
 *      dimensions are in em units, to take advatage of the browser's existing
 *      ability to turn pixel dimension into a larger grid through em sizes.
 *      CSS classes with definitions for the objects in the game are assumed to
 *      exist.
 *
 *  Author: Mike Gossmann
 *  Date: 2010/11/14 2010/11/19
 *
 */

//Global variables for the game engine
var keyTrackers = new Array; // Stores a reference to every object that tracks the keyboard
 
function Engine(obj, gridSize, displayWidth, displayHeight, startMap){
    // This object will be used to create and store many usefull values and
    // tools for working with the game. obj is the ID of the object to use as
    // the game display. gridsize is the side-length of a grid square, in px.
    // displayWidth and displayHeight are the dimensions of the diplay area, in em.
    // startMap is the first map to load.

    // Store important values for the game
    this.gridSize = gridSize; // The size of both sides of each grid square
    this.display = document.getElementById(obj); // The object used to display the game
    this.display.dim = {'x':displayWidth, 'y':displayHeight}; // The dimensions of the display
    this.levelNum = startMap; // Store the current level.
    this.reloadMap = false; // If this is true the game will reload the map at the next update
    this.mapLoaded = false; // This is false while downloading the map
    this.mapSettings; // A storage container for the map settings
    this.goalsMet = 0; // Keep track of how many goals are met in a level.

    // Initialize the display
    with (this.display){
        // This stuff should alredy be set in the CSS, but it's verry important
        // so it's being explicitly set just to be safe.
        style.fontSize = this.gridSize+"px"; // use the font size to make em units act as a grid
        style.width = dim.x + "em";
        style.height = dim.y+"em";
        innerHTML = ""; // Erase anything that might have already been in the game object
    }

    // Create storage space for the actors
    this.actors = new Array;

    // Some initialization methods
    this.loadMap = function(mapSettings){
        // This function takes a map settings object and builds a level

        // Remove the old map if it's there
        if (this.map) this.display.removeChild(this.map.container);

        // Dump the old actors
        if (this.actors.length > 0){
            this.actors = new Array;
        }
        
        // Create the map, draw the blocks and configures everything
        this.map = new Map(mapSettings);

        // Put the map in the display
        this.display.appendChild(this.map.container);

        // Load the actors
        for (var i=0; i < this.map.actorsStart.length; i++){
            this.addActor(new this.map.actorsStart[i][0](this, this.map.actorsStart[i][1]));
        }

        // Create the player
        this.player = new Player(this, {
            'gPos': {'x':this.map.start.x, 'y':this.map.start.y},
            'speed': new Vector(0, 0, 50),
            'solid': true,
            'controller': new Controller(keyTrackers)
        });
        this.addActor(this.player);

    }

    this.requestMap = function(levelNum){
        // This function uses ajax to grab the requested level from the server
        if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        }else{// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        g = this; // Temporary reference to the game itself
        // Specify what to do with the map when we get it
        xmlhttp.onreadystatechange=function(){
            if (xmlhttp.readyState==4 && xmlhttp.status==200){
                g.mapSettings = eval("("+xmlhttp.responseText+")");
                g.mapLoaded = true;
                g.reloadMap = true;
            }else if(xmlhttp.readyState==4 && xmlhttp.status == 404){
                alert("Congratulations! You Win!");
            }
        }
        // Request the map
        this.mapLoaded = false;
        var d = new Date();
        xmlhttp.open("GET","maps/level"+levelNum+".js?t="+d.getTime(),true);
        xmlhttp.send();
        
    }

    // Some usefull methods
    this.toGrid = function(pPos){
        // This function converts a pixel coordinate into grid coordinate.
        // pPos is a basic object with an "x" property and a "y" property
        // The funtion returns a similair object, with the converted coordinates
        // It rounds in the direction of gravity to stick things to the floor
        // better.
        switch (this.map.gravity.dir){
            case 1:
                return {"x" : Math.floor(Math.ceil(pPos.x)/this.gridSize), "y" : Math.floor(Math.round(pPos.y)/this.gridSize)};
            case 3:
                return {"x" : Math.floor(Math.round(pPos.x)/this.gridSize), "y" : Math.floor(Math.floor(pPos.y)/this.gridSize)};
            case 5:
                return {"x" : Math.floor(Math.floor(pPos.x)/this.gridSize), "y" : Math.floor(Math.round(pPos.y)/this.gridSize)};
            case 7:
                return {"x" : Math.floor(Math.round(pPos.x)/this.gridSize), "y" : Math.floor(Math.ceil(pPos.y)/this.gridSize)};
            default:
                return {"x" : Math.floor(Math.round(pPos.x)/this.gridSize), "y" : Math.floor(Math.round(pPos.y)/this.gridSize)};
        }
    }
    this.getSquares = function(pPos){
        // This function assumes that pPos represents the top-left corner of an
        // object that is one gridsize in each dimension. It then returns the
        // grid coordinates for each corner of the object.
        sqs = new Object;
        sqs.tl = this.toGrid({'x': pPos.x, 'y': pPos.y});
        sqs.tr = this.toGrid({'x': pPos.x+this.gridSize-1, 'y': pPos.y});
        sqs.bl = this.toGrid({'x': pPos.x, 'y': pPos.y+this.gridSize-1});
        sqs.br = this.toGrid({'x': pPos.x+this.gridSize-1, 'y': pPos.y+this.gridSize-1});
        return sqs;
    }
    this.addActor = function(actor){
        // This function adds the given actor to the actor's array.
        this.actors.push(actor);
        this.map.layers.actors.appendChild(actor.html); // add the HTML too
    }
    this.removeActor = function(actor){
        this.map.layers.actors.removeChild(actor); // remove the HTML
        i = this.actors.IndexOf(actor); // find where the actor is in the array
        if (i!=-1) this.actors.splice(i, 1); // cut it out,if it's there.
    }
    this.move = function(position, speed){
        // Creates a new Position object using a speed object
        return {'x':position.x+speed.x, 'y':position.y+speed.y};
    }
    this.update = function(){
        // This method goes through all the actors in the actor array and
        // Updates them.
        // Check if we should reload the map
        if (this.reloadMap){
            this.reloadMap = false;
            this.loadMap(this.mapSettings);
        }else if(this.mapLoaded){
            this.goalsMet = 0; // reset the goal counter
            for (var i=0; i<this.actors.length; i++){
                this.actors[i].update();
                if (this.actors[i].type=="goal" && this.actors[i].achieved ) this.goalsMet++;
            }
            //Move the camera
            this.map.follow(this.player, 100, 100);
            
            // Check if we've beat the level
            if(this.goalsMet >= this.map.goals){
                this.levelNum ++;
                this.requestMap(this.levelNum);
            }
        }
    }

    // Event binding
    window.onkeydown = function(e){
        // This function runs every time a key is pressed. It runs through the
        // objects in KeyTrackers and checks if they have bindings for the key
        // that's being pressed. If they do it updates the object and stops
        // the key from doing anything to the browser.

        //Get the key that's being pressed
        if (!e) e = window.event; // IE doesn't pass e, so get it
        key = (e.keyCode ? e.keyCode : e.which) // again, compensate for IE
        if (key == 82) game.reloadMap = true; // the R key retry's a level
        for (var i=0; i<keyTrackers.length; i++){
            if (key in keyTrackers[i].mappings){
                e.preventDefault(); // Stops the keys from scrolling the page and stuff
                keyTrackers[i][keyTrackers[i].mappings[key]] = true; // Stores that the key is down
            }
        }
    }
    window.onkeyup = function(e){
        // This function is identical to the previous funtion, but checks for
        // the key being released, not pressed.

        //Get the key that's being pressed
        if (!e) e = window.event; // IE doesn't pass e, so get it
        key = (e.keyCode ? e.keyCode : e.which) // again, compensate for IE
        for (var i=0; i<keyTrackers.length; i++){
            if (key in keyTrackers[i].mappings){
                e.preventDefault(); // Stops the keys from scrolling the page and stuff
                keyTrackers[i][keyTrackers[i].mappings[key]] = false; // Stores that the key is up
            }
        }
    }

    this.requestMap(this.levelNum);
}

function Vector(speed, angle, limit){
    // This object represents a vector. The primary use of a vector in this
    // engine is adjusting positions. The speed and angle are used to create a
    // speedX and speedY. Speed is in px/per 10 frames, and angle is in degrees,
    // 0 degrees being due right, and 90 degrees being straight up.
    // A negative speed can be given, but then the angle will be adjusted to
    // give a postive speed on the final vector.

    // Create/Store the starting properties
    this.speed = speed;
    this.angle = angle;
    this.dir = 0;
    this.limit = limit;

    // Set defaults for the properties we are about to calculate
    this.speedX = 0;
    this.speedY = 0;

    // Calculation Methods
    this.reCalculate = function(){
        // This is possibly the most important method of the Vector object. It
        // Takes all the properties of the vecotor and works out the new speeds.

        // Keep the speed positive
        if (this.speed < 0){
            this.speed *= -1;
            this.angle += 180;
        }

        //Cap the speed
        if (this.limit !== false){
            if(this.speed > this.limit){
                this.speed = this.limit;
            }
        }

        // Limit the angle
        this.angle %= 360;
        this.angle += (this.angle < 0 ? 360 : 0);

        //The trigonometry.
        this.speedX = Math.cos((this.angle)*(Math.PI/180))*this.speed/10;
        //The vertical speed is inverted because our coordinate system is flipped vertically
        this.speedY = (Math.sin((this.angle)*(Math.PI/180))*this.speed/10)*-1;

        // Work out the direction
        this.findDir();

    }
    this.reverseCalculate = function(){
        // This function works out the speed anc angle values fromt the speedX
        // and speedY values.

        this.speed = Math.sqrt( Math.pow(this.speedX,2) + Math.pow(this.speedY,2) )*10;

        // The angle needs a little extra code to make sure we don't divide by zero
        if (this.speedX == 0 && this.speedY == 0){
            this.angle = 180;
        }else if(this.speedX == 0){
            this.angle = (this.speedY > 0 ? 270 : 90);
        }else if(this.speedY == 0){
            this.angle = (this.speedX > 0 ? 0 : 180);
        }else{
            this.angle = Math.atan((this.speedY*-1)/this.speedX)*(180/Math.PI) + (this.speedX > 0 ? 0 : 180);
            this.angle %= 360;
            this.angle += (this.angle < 0 ? 360 : 0);
        }

        // Work out the direction
        this.findDir();

        if (this.limit !== false){
            if(this.speed > this.limit){
                this.speed = this.limit;
                this.reCalculate();
            }
        }
    }
    this.findDir = function(){
        // Uses the angle to work out the direction to one of 9 possibilities
        // 0 = not moving, 1 = right, 2 = up+right, 3 = up, and so on.
        if (this.speed > 0){
            if(this.angle <= 22.5){
                this.dir = 1;
            }else if(this.angle <=67.5){
                this.dir = 2;
            }else if (this.angle <= 112.5){
                this.dir = 3;
            }else if (this.angle <= 157.5){
                this.dir = 4;
            }else if (this.angle <= 202.5){
                this.dir = 5;
            }else if (this.angle <= 247.5){
                this.dir = 6;
            }else if (this.angle <= 292.5){
                this.dir = 7;
            }else if (this.angle <= 337.5){
                this.dir = 8;
            }else{
                this.dir = 1;
            }
        }else{
            this.dir = 0;
        }
    }

    // Adjustment methods
    this.adjust = function(speed, angle, limit){
        // This function modifies the speed and angle of the Vector, then redoes
        // the math.
        this.speed += speed;
        this.angle += angle;
        this.limit = (this.limit === false ? false : this.limit + limit);
        this.reCalculate();
    }
    this.accelerate= function(speed){
        // This function calls the adjust function, but assumes the current angle and limt
        this.adjust(speed, 0, 0);
    }
    this.rotate = function(angle){
        // This function calls the adjust function, but assumes the current speed and limit
        this.adjust(0, angle, 0);
    }
    this.adjustLimit = function(limit){
        // This function calls the set limit function with a new limit based on the old limit.
        this.setLimit(this.limit+limit);
        // See if we need to act on the new limit
        if (this.speed > this.limit) this.reCalculate();
    }

    // Setting methods
    this.set = function(speed, angle){
        // This function changes the speed and angle of the Vector, then redoes
        // the math.
        this.speed = speed;
        this.angle = angle;
        this.reCalculate();
    }
    this.setSpeed = function(speed){
        // This function calls the set function, but assumes the current angle
        this.set(speed, this.angle);
    }
    this.setAngle = function(angle){
        // This function calls the set function, but assumes the current speed
        this.set(this.speed, angle);
    }
    this.setLimit = function(limit){
        // Changes the speed limit to a new value, and checks if the vector needs to slow down
        this.limit = limit;
        if (this.speed > this.limit) this.reCalculate();
    }

    // Change vector based on a Speed object
    this.adjustBySpeed = function(speedObj){
        // Uses a speed object to adjust the vector's speed and angle
        this.speedX += speedObj.x;
        this.speedY += speedObj.y;
        this.reverseCalculate();
    }
    this.setToSpeed = function(speedObj){
        // Uses a speed object to change the vector's speed and angle
        this.speedX = speedObj.x;
        this.speedY = speedObj.y;
        this.reverseCalculate();
    }

    // Vector math, but not really
    this.add = function(vector){
        // Adds two vectors together, the easy way using the speeds
        this.adjustBySpeed(vector.getSpeedObj());
    }
    this.subtract = function(vector){
        // Similair to the above, but reversing the speeds first
        speedObj = vector.getSpeedObj();
        this.adjustBySpeed({'x':speedObj.x*-1, 'y':speedObj.x*-1});
    }

    // Get a Speed Object from the vector
    this.getSpeedObj = function(){
        return {'x': this.speedX, 'y':this.speedY}
    }
    this.relativeAngle = function(angle){
        // Returns what angle would be if this.angle is treated as 0deg.
        angle -= this.angle;
        angle %= 360;
        return angle + (angle < 0 ? 360 : 0);
    }
    this.relativeAngleP = function(angle){
        // Same as a above, but relative to an angle 90 clockwise of this.angle
        // thus treating this.angle as pointing straight down (this function is
        // useful for gravity math)
        angle -= this.angle+90;
        angle %= 360;
        return angle + (angle < 0 ? 360 : 0);
    }

    // Do the math
    this.reCalculate();

}

function Controller(tracker, mappings){
    // This object represents the controller. In it's current state it only
    // supports they Keyboard as a controller. The button mappings are
    // coded in. tracker is the object that can update the keystates.

    // These are the control mappings
    this.mappings = {
        38 : 'up',
        39 : 'right',
        40 : 'down',
        37 : 'left',
        32 : 'jump',
        90 : 'attack',
        16 : 'kick'
    };

    if (mappings) this.mappings = mappings;

    // Create the properties that store the keystates
    this.up = false;
    this.right = false;
    this.down = false;
    this.left = false;
    this.jump = false;
    this.attack = false;
    this.kick = false;

    // Add this controller to the tracker
    tracker.push(this);

}

function Actor(game, settings){
    // This is the parent class for everything in the game that has to be able
    // to move. The only parameter it accepts is what game it's an actor in.

    // The properties passed through settings are:
    // pPos: the starting position, in pixels
    // speed: A vector for the actors movement
    // solid: boolean: a solid actor responds to gravity and friction and collisions

    // Store a reference to the game itself.
    this.game = game;

    // Store values from settings, extrapolate the other important information
    for (var i in settings){
        this[i] = settings[i]
    }
	this.type = "basic";
    this.html = document.createElement('div');
    this.className = 'basic';
    // Turn the basic gPos given into a pPos, then use that to make the more
    // complicate 4-corner gPos
    this.pPos = new Object;
    this.pPos.x = this.gPos.x*this.game.gridSize;
    this.pPos.y = this.gPos.y*this.game.gridSize;
    this.gPos = game.getSquares(this.pPos);
    this.onGround = false // A way to check if we need can jump
    this.onWallRight = false; // A way to check if we can wall-jump
    this.onWallLeft = false;

    this.update = function(){
        // This function is called by the game engine every frame. It works in
        // 3 steps. First, changes are made to speed vector based on any factors
        // that need to be considered. Next, collsiion checking with the blocks
        // on the map is done, adjusting the speed vector appropriately. Finally,
        // the Actor is actually moved.

        // We use this a lot, and it's a pain to do the full reference.
        var grav = this.game.map.gravity;

        if (this.solid){
            this.speed.add(grav); // Apply the gravity

            // Get the testing values
            var testPos = this.game.move(this.pPos, this.speed.getSpeedObj());
            var gridSquares = this.game.getSquares(testPos);

            var actorCollisions = new Array();

            // Create the testSquares, this is a copy of gridSquares that's been
            // corrected for the direction of gravity. In the event of a diagonal
            // gravity, the .tl value hold the top of the actor, and everything else
            // follows clockwise as it normall would.
            var testSquares = new Object();
            var horz; // stores the horizontal axis
            var vert; // stores the vertical axis
            var p; // boolean: whether gravity is towards the positive or negative
            switch (grav.dir){
                // The 90 degree angles
                case 1: case 2: // Gravity is from +X, Gravity from +X -Y
                    testSquares.tl = gridSquares.bl;
                    testSquares.bl = gridSquares.br;
                    testSquares.tr = gridSquares.tl;
                    testSquares.br = gridSquares.tr;
                    horz = 'y';
                    vert = 'x';
                    p = true; // Gravity towards the positive 
                break;
                case 3: case 4: // Gravity is from -Y, Graviry from -X -Y
                    testSquares.tl = gridSquares.br;
                    testSquares.bl = gridSquares.tr;
                    testSquares.tr = gridSquares.bl;
                    testSquares.br = gridSquares.tl;
                    horz = 'x';
                    vert = 'y';
                    p = false;
                break;
                case 5: case 6: // Gravity is from -X, Graviry from -X +Y
                    testSquares.tl = gridSquares.tr;
                    testSquares.bl = gridSquares.tl;
                    testSquares.tr = gridSquares.br;
                    testSquares.br = gridSquares.bl;
                    horz = 'y';
                    vert = 'x';
                    p = false;
                break;
                case 7: case 8: default: // Gravity is from +Y, Gravity from +X +Y, no Gravity
                    testSquares.tl = gridSquares.tl;
                    testSquares.bl = gridSquares.bl;
                    testSquares.tr = gridSquares.tr;
                    testSquares.br = gridSquares.br;
                    horz = 'x';
                    vert = 'y';
                    p = true;
            }

            // Get a list of intersetions with blocks and setup for actor collisions
            testSquares.tl.block = this.game.map.isBlockAt(testSquares.tl)
            testSquares.bl.block = this.game.map.isBlockAt(testSquares.bl)
            testSquares.tr.block = this.game.map.isBlockAt(testSquares.tr)
            testSquares.br.block = this.game.map.isBlockAt(testSquares.br)


            // Check for colisions. The directions are relative to gravity
            var collisions = new Object();
            collisions.above = false;
            collisions.right = false;
            collisions.below = false;
            collisions.left = false;

            var v = 't'; // Stores the vertical direction prefix for the horizontal checks;
            var a = grav.relativeAngleP(this.speed.angle); // Stores the actors angle of movement relative to gravity
            if (grav.dir != 0){ // actually moving?
                // Is the actor moving up or down?
                v = (a < 360 && a > 180 ? 't' : 'b')

                if (testSquares.tl.block && testSquares.tr.block){
                    collisions.above = true;
                }else{
                    collisions.above = (
                        (p ? testPos[vert]%this.game.gridSize>this.game.gridSize-this.game.gridSize/4 : testPos[vert]%this.game.gridSize<this.game.gridSize/4)
                        && v=='b'
                        && (testSquares.tl.block || testSquares.tr.block)
                        && !(testSquares.bl.block || testSquares.br.block)
                    );
                }

                if (testSquares.bl.block && testSquares.br.block){
                    collisions.below = true;
                }else{
                    collisions.below = (
                        (p ? testPos[vert]%this.game.gridSize<this.game.gridSize/4 : testPos[vert]%this.game.gridSize>this.game.gridSize-this.game.gridSize/4)
                        && v=='t'
                        && (testSquares.bl.block || testSquares.br.block)
                        && !(testSquares.tl.block || testSquares.tr.block)
                    );
                }

                // Is the actor moving left or right?
                if (a < 90 || a > 270){
                    // Going right
                        if ((collisions.below || collisions.above) && testSquares[v+'r'].block){
                            collisions.right = true;
                        }else if(!collisions.below && !collisions.above && (testSquares.tr.block || testSquares.br.block)){
                            collisions.right = true;
                        }
                }else if (a < 270 && a > 90){
                    // Going left
                        if ((collisions.below || collisions.above) && testSquares[v+'l'].block){
                            collisions.left = true;
                        }else if (!collisions.below && !collisions.above && (testSquares.tl.block || testSquares.bl.block)){
                            collisions.left = true;;
                        }
                }

            }

            // Now comes the time to actually react to all the colliding going on.
            // but only if the object is solid.
            if (this.speed.dir != 0){
                //Check the floor
                if (collisions.below){
                    // We've hit the floor! Reposition!
                    this.speed['speed' + vert.toUpperCase()] = 0;
                    this.speed.reverseCalculate();
                    testPos[vert] = testSquares.tl[vert]*this.game.gridSize;
                    this.onGround = true;
                    //Friction!
                    if (this.speed.speed > 2){
                        this.speed.accelerate(-2);
                    }else{
                        this.speed.setSpeed(0);
                    }
                }else this.onGround = false;
                // Check the ceiling
                if (collisions.above){
                    this.speed['speed' + vert.toUpperCase()] = 0;
                    this.speed.reverseCalculate();
                    testPos[vert] = testSquares.bl[vert]*this.game.gridSize;
                }
                // Check the wall to the right
                if (collisions.right){
                    testPos[horz] = testSquares.tl[horz]*this.game.gridSize;
                    this.speed['speed' + horz.toUpperCase()] = 0;
                    this.speed.reverseCalculate();
                    this.onWallRight = true;
                }else this.onWallRight = false;
                // Check the wall to the left
                if (collisions.left){
                    testPos[horz] = testSquares.tr[horz]*this.game.gridSize;
                    this.speed['speed' + horz.toUpperCase()] = 0;
                    this.speed.reverseCalculate();
                    this.onWallLeft = true;
                }else this.onWallLeft = false;
            }

            // Actually move
            this.pPos = testPos;
            this.gPos = game.getSquares(this.pPos);

        }

        // Update the CSS
        with (this.html.style){
            left = Math.round(this.pPos.x)+"px"; // Actors can break free from the grid
            top = Math.round(this.pPos.y)+"px";  // so their position is set in px.
        }
    }

}

function Map(settings){
    // This object essentially describes a level in the game. It includes a map
    // of the level, positions for various things, the gravity, start location,
    // location of the end, etc.

    // The properties passed through settings are:
    // gravity: a vector describing the level's gravity
    // start: the gPos where the player starts
    // width: The width of the map in grid units
    // height: the height of the map in grid units
    // background: a css declaration for the background;
    // blocks: a 2-dimensional array describing where all the blocks are.
    // actors: an array containing the contructor funtions and settins for the actors
    // goals: the number of goals that must be met to clear the level

    // Load all the options from the settings object into the world
    for (var i in settings){
        this[i] = settings[i]
    }

    //Clone the gravity to avoid bugs late
    this.gravity = new Vector(settings.gravity.speed, settings.gravity.angle, settings.gravity.limit);

    this.camPos = {'x':0, 'y':0}; // Stores the position of the "camera"

    // Create the main HTML container for the map
    this.container = document.createElement('div');
    with (this.container){
        className = "gameWorld";
        style.width = this.width + "em";
        style.height = this.height + "em";
        style.background = this.background;
    }

    //Create the individual layers
    this.layers = new Object;
    this.layers.blocks = document.createElement('div'); // Layer for blocks
    this.layers.actors = document.createElement('div'); // Layer for actors

    //Configure them and add them to the container
    for (i in this.layers){
        this.layers[i].className = "layer";
        this.container.appendChild(this.layers[i]);
    }

    // Methods
    this.drawBlocks = function (layer){
        // This function creates all the blocks in the layer div

        // Clear the current contents
        layer.innerHTML = "";

        //Make the blocks
        for (var x=0; x<this.width; x++){
            for (var y=0; y<this.height; y++){
                if (this.blocks[x][y] == 1){
                    newBlock = document.createElement('div');
                    newBlock.className = "block";
                    newBlock.style.left = x+"em";
                    newBlock.style.top = y+"em";
                    layer.appendChild(newBlock);
                }
            }
        }
    }
    this.isBlockAt = function(position){
        // This funtion takes a gPos object and returns whether or not there's a block
        x = position.x;
        y = position.y;
        //Check if the point's on the grid, if not say there's a block there
        if(x < 0 || x > this.width-1 || y < 0 || y > this.height-1) return true;

        // Check if there's a block at the point
        if (this.blocks[x][y] == 1) return true;
        else return false;
    }
    this.follow = function(actor, tolX, tolY){
        // This function positions the main window so that the specified actor
        // is in the center of the screen. tolX and tolY determine how far from
        // the center of the screen the actor is allowed to move. What this
        // function is actually doing is moving the container div within the
        // display div.

        // Check along X
        if ((actor.pPos.x < this.camPos.x + (actor.game.display.dim.x*actor.game.gridSize/2) - tolX && actor.speed.speedX < 0)
            || (actor.pPos.x > this.camPos.x + (actor.game.display.dim.x*actor.game.gridSize/2) + tolX && actor.speed.speedX > 0)
        ){
            this.camPos.x += actor.speed.speedX;
        }

        // Check along y
        if ((actor.pPos.y < this.camPos.y + (actor.game.display.dim.y*actor.game.gridSize/2) - tolX && actor.speed.speedY < 0)
            || (actor.pPos.y > this.camPos.y + (actor.game.display.dim.y*actor.game.gridSize/2) + tolX && actor.speed.speedY > 0)
        ){
            this.camPos.y += actor.speed.speedY;
        }
		
		// Fix the camera if the actor gets outside of the camera
		if (actor.pPos.x < this.camPos.x || actor.pPos.x > this.camPos.x + (actor.game.display.dim.x*actor.game.gridSize)) this.camPos.x = actor.pPos.x - (actor.game.display.dim.x*actor.game.gridSize/2);
		if (actor.pPos.y < this.camPos.y || actor.pPos.y > this.camPos.y + (actor.game.display.dim.y*actor.game.gridSize)) this.camPos.y = actor.pPos.y - (actor.game.display.dim.y*actor.game.gridSize/2);


        // Constrain the camera to the map
        //if (this.camPos.x < 0) this.camPos.x = 0;
        //if (this.camPos.x > (this.width - actor.game.display.dim.x) * actor.game.gridSize) this.camPos.x = (this.width - actor.game.display.dim.x) * actor.game.gridSize;
        //if (this.camPos.y < 0) this.camPos.y = 0;
        //if (this.camPos.y > (this.height - actor.game.display.dim.y) * actor.game.gridSize) this.camPos.y = (this.height - actor.game.display.dim.y) * actor.game.gridSize;

        // Position the camera
        with (this.container.style){
            left = (this.camPos.x)*-1+"px"; // We multiply by -1 because the camera
            top = (this.camPos.y)*-1+"px"; // stays still, while the map moves
        }
    }

    this.drawBlocks(this.layers.blocks);

}








/* ROOOM */