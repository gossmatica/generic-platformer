/*  game.js
 *
 *  Description:
 *      This file uses the classes defined in engine.js to put together an
 *      actual game. It defines a player, and special blocks and other things.
 *
 *  Author: Mike Gossmann
 *  Date: 2010/11/18 2010/11/19
 *
 */

// Get the game running!

var t; // global for the timout for the main loop of the game

window.onload = function (){

    // This function get's called as soon as there's HTML to work with. It loads
    // whatever info is needed to start the game, and then starts it.

    // Debuging span, very useful. Alerts in a 30-milisecond loop are hell.
    debug = document.getElementById('debug');

    // Load and configure the game!
    game = new Engine("game", 32, 20, 15, 1);

    // Start the mnain loop
    doUpdate();

}

function doUpdate(){

    //Call the game's update function
    game.update();

    // Do this all again in 30 milliseconds.
    t = setTimeout("doUpdate()", 30);
}

//----------------------------- More Game Classes ----------------------------//
 
//The player class, inherits from the actor class
function Player(game, settings){
    // This object is a player character. It is controlled by the player and
    // has movement controlled by the keyboard.

    // On top of the settings a normal Actor requires, the Player object also
    // excepts a few others:
    // controller: The controller object bound to the player
    // target: whether or not the object should 

    // Set up the parent
    this.prototype = new Actor(game, settings);
    
    // Call the parent constructor
    this.prototype.constructor.call(this, game, settings);
	
	//Overide the type
	this.type = 'player';
    this.html.className = 'player';

    //Create the player's sprite
    this.sprite = document.createElement('img');
    this.html.appendChild(this.sprite);
    this.sprite.src = "images/characterSprites/jump.gif";
    this.airTick = 0; // Used to smooth out some glitchy sprite stuff

    // The update function
    this.update = function(){
        // This function is called every frame by the engine. First it handles
        // the players controls, and object collisions, then it passes controll
        // off to the Actor update function to handle gravity, floors, walls,
        // ceilings, friction, etc.

        // Jumping
        if (this.controller.jump && this.onGround){
            this.speed.add(new Vector(35, this.game.map.gravity.angle+180, false));
        }else if(this.controller.jump && this.onWallRight && !this.onGround){
            this.speed.set(45, this.game.map.gravity.angle+225);
        }else if(this.controller.jump && this.onWallLeft && !this.onGround){
            this.speed.set(45, this.game.map.gravity.angle+135);
        }

        // Walking
        if (this.controller.left && this.onGround){
            this.speed.add(new Vector(3, this.game.map.gravity.angle-90, false));
        }else if (this.controller.left){
            this.speed.add(new Vector(1, this.game.map.gravity.angle-90, false));
        }
        if (this.controller.right && this.onGround){
            this.speed.add(new Vector(3, this.game.map.gravity.angle+90, false));
        }else if (this.controller.right){
            this.speed.add(new Vector(1, this.game.map.gravity.angle+90, false));
        }

        // Drop faster
        if (this.controller.down && !this.onGround){
            this.speed.add(new Vector(10, this.game.map.gravity.angle, false));
        }

        // Gravity Spin
        if(this.controller.attack && this.onWallRight){
            this.game.map.gravity.rotate(90);
            this.speed.setSpeed(0);
        }else if(this.controller.attack && this.onWallLeft){
            this.game.map.gravity.rotate(-90);
            this.speed.setSpeed(0);
        }

        // Allow for collisions with other actors
        for (var j=0; j < this.game.actors.length; j++){
            if (this.game.actors[j] != this){
                // Check if the two actors actually overlap
                aX = this.game.actors[j].pPos.x;
                aY = this.game.actors[j].pPos.y;
                aW = this.game.actors[j].html.offsetWidth;
                aH = this.game.actors[j].html.offsetHeight;
                if (
                    ((this.pPos.x >= aX && this.pPos.x < aX + aW)
                        ||(this.pPos.x + this.game.gridSize > aX && this.pPos.x + this.game.gridSize < aX + aW)
                    )
                    && ((this.pPos.y >= aY && this.pPos.y < aY + aH)
                        ||(this.pPos.y + this.game.gridSize > aY && this.pPos.y + this.game.gridSize < aY + aH)
                    )
                ){
                    // Collisions with boxes
                    if (this.game.actors[j].type == "box"){
                        if (this.controller.up && this.controller.kick){
                            this.game.actors[j].speed.set(30, this.game.map.gravity.angle+90+(Math.floor(Math.random()*180)));
                        }else if (this.controller.up){
                            this.game.actors[j].speed.set(30, this.game.map.gravity.angle+180);
                        }else if (this.controller.kick){
                            tx = this.game.actors[j].pPos.x - this.pPos.x;
                            ty = this.game.actors[j].pPos.y - this.pPos.y;
                            this.game.actors[j].speed.setToSpeed({'x':tx, 'y':ty});
                        }
                    }else if(this.game.actors[j].type == "hazard"){
                        this.game.reloadMap = true;
                    }
                }
            }
        }

        this.prototype.update.call(this);

        // Set up the sprite
        var d = this.game.map.gravity.dir;
        var newSrc = this.sprite.src;
        if (this.onWallRight){
            newSrc = "images/characterSprites/rightWall"+d+".gif";
            this.airTick = 0;
        }else if (this.onWallLeft){
            newSrc = "images/characterSprites/leftWall"+d+".gif";
            this.airTick = 0;
        }else if (this.onGround){
            newSrc = "images/characterSprites/ground"+d+".gif";
            this.airTick = 0;
        }else if (this.airTick == 3){
            newSrc = "images/characterSprites/jump.gif";
            this.airTick = 0;
        }else this.airTick++;
        this.sprite.src = newSrc;

    }


}

// This class represents a basic block and inherits from the actor class
function Box(game, settings){
    // This class doesn't really deviate much from the Actor class, but it has
    // a different type so other objects react differently
    
	// Set up the parent
    this.prototype = new Actor(game, settings);
    
    // Call the parent constructor
    this.prototype.constructor.call(this, game, settings);

    this.type = 'box';
    this.html.className = "box";


     // The update function
    this.update = function(){
        // This function is called every frame by the engine. First it handles
        // hazard collisions, then it passes controll off to the Actor update
        // function to handle gravity, floors, walls, ceilings, friction, etc.

        // Allow for collisions with other actors
        for (var j=0; j < this.game.actors.length; j++){
            if (this.game.actors[j] != this && this.game.actors[j].type=="telehazard"){
                // Check if the two actors actually overlap
                aX = this.game.actors[j].pPos.x;
                aY = this.game.actors[j].pPos.y;
                aW = this.game.actors[j].html.offsetWidth;
                aH = this.game.actors[j].html.offsetHeight;
                if (
                    ((this.pPos.x >= aX && this.pPos.x < aX + aW)
                        ||(this.pPos.x + this.game.gridSize > aX && this.pPos.x + this.game.gridSize < aX + aW)
                    )
                    && ((this.pPos.y >= aY && this.pPos.y < aY + aH)
                        ||(this.pPos.y + this.game.gridSize > aY && this.pPos.y + this.game.gridSize < aY + aH)
                    )
                ){
                    // Teleport the blocks somewhere else
                    this.pPos.x = this.game.actors[j].out.x*this.game.gridSize;
                    this.pPos.y = this.game.actors[j].out.y*this.game.gridSize;
                    this.speed.rotate(this.game.actors[j].outAngle)
                }
            }
        }

        this.prototype.update.call(this);

    }
}

// This is a hazard, if the player falls in it, they have to restart, inheits from actor
function Hazard(game, settings){
    // This object is a danger to the player, if the player falls in, the level
    // resets

    // It's setting object also accepts a width and a height, in grid units.
    
    // Set up the parent
    this.prototype = new Actor(game, settings);
    
    // Call the parent constructor
    this.prototype.constructor.call(this, game, settings);
    
    this.type = 'hazard';
    this.html.className = "hazard";

    this.html.style.width = this.width + "em";
    this.html.style.height = this.height + "em";

}

// This hazzard-based object teleports anything boxes hit it.
function Telehazard(game, settings){
    // This object is a danger to the player, if the player falls in, the level
    // resets

    // It's setting object also accepts a width and a height, in grid units.
    // Along with:
    // out : a gGpos object telling the engine where to teleport blocks
    // outAngle: an angle to use to adjust the vectors on the boxes exiting
    
    // Set up the parent
    this.prototype = new Actor(game, settings);
    
    // Call the parent constructor
    this.prototype.constructor.call(this, game, settings);
    
    this.type = 'telehazard';
    this.html.className = "telehazard";

    this.html.style.width = this.width + "em";
    this.html.style.height = this.height + "em";

}

// This object lets you actually beat a level, inherits from actor
function Goal(game, settings){
    // This object accepts the normal settings, plus 'amount', which represents
    // the number of blocks that need to be placed on this goal to activate it.
    // Once all the goals are activated, the next level can load. A goal can be
    // as large or small as it likes.

    // Set up the parent
    this.prototype = new Actor(game, settings);
    
    // Call the parent constructor
    this.prototype.constructor.call(this, game, settings);
    
    this.type = 'goal';
    this.html.className = "goal";

    this.boxes = 0; // Keep track of the number of blocks that are in this goal
    this.achieved = false; // Keep track of if the goal has been met or not.

    this.html.style.width = this.width + "em";
    this.html.style.height = this.height + "em";
    this.html.style.lineHeight = this.height*this.game.gridSize + "px";
    this.html.innerHTML =this.boxes + "/" + this.amount;

    // The update function
    this.update = function(){
        // This function is called every frame by the engine. First it handles
        // collisions with the blocks, then it passes on cotrol to the protype

        //Reset the blocks counter
        this.boxes = 0;

        // Allow for collisions with boxes
        for (var j=0; j < this.game.actors.length; j++){
            if (this.game.actors[j] != this && this.game.actors[j].type=='box'){
                // Check if the two actors actually overlap
                aX = this.game.actors[j].pPos.x;
                aY = this.game.actors[j].pPos.y;
                aW = this.game.actors[j].html.offsetWidth;
                aH = this.game.actors[j].html.offsetHeight;
                if (
                    ((aX >= this.pPos.x && aX <= this.pPos.x + this.html.offsetWidth)
                        ||(aX + aW >= this.pPos.x  &&  aX + aW <= this.pPos.x + this.html.offsetWidth)
                    )
                    && ((aY >= this.pPos.y && aY <= this.pPos.y + this.html.offsetHeight)
                        ||(aY + aH >= this.pPos.y  &&  aY + aH <= this.pPos.y + this.html.offsetHeight)
                    )
                ){
                    // Collisions with boxes
                    this.boxes ++;
                }
            }
        }

        //Update the blocks display
        this.html.innerHTML =this.boxes + "/" + this.amount;
        if(this.boxes == this.amount){
            this.html.style.color = "#ffffff";
            this.html.style.backgroundColor = "#00ff00";
            this.achieved = true;
        }else if(this.boxes > this.amount){
            this.html.style.color = "#00ff00";
            this.html.style.backgroundColor = "#B686DE";
            this.achieved = false;
        }else{
            this.html.style.color = "#ff0000";
            this.html.style.backgroundColor = "#B686DE";
            this.achieved = false;
        }

        this.prototype.update.call(this);

    }
}