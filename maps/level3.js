{
    'gravity': new Vector(1, 270, false),
    'start': {'x':9, 'y':9},
    'width': 10,
    'height':10,
    'background':"#cceeff",
    'goals': 2,
    'blocks': [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        [1, 0, 0, 1, 1, 1, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ],
    'actorsStart' : [
        [Box, {
            'gPos': {'x':0, 'y':9},
            'speed': new Vector(0, 0, 50),
            'solid': true
        }],
        [Box, {
            'gPos': {'x':1, 'y':9},
            'speed': new Vector(0, 0, 50),
            'solid': true
        }],
        [Hazard, {
            'gPos' : {'x':0, 'y':3},
            'speed' : new Vector(0,0,0),
            'solid' : false,
            'width' : 10,
            'height' : 1
        }],
        [Goal, {
            'gPos' : {'x':3, 'y':4},
            'speed' : new Vector(0, 0, 0),
            'solid' : false,
            'width' : 3,
            'height' : 1,
            'amount' : 1
        }],
        [Goal, {
            'gPos' : {'x':0, 'y':0},
            'speed' : new Vector(0, 0, 0),
            'solid' : false,
            'width' : 2,
            'height' : 1,
            'amount' : 1
        }]
    ]
}